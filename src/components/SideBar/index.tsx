
import { Grid, TextField } from "@mui/material";

type filterData = {
  user_name: string,
  user_phone: string,
  country: string,
  date: string
}

function SideBar(filter_data: filterData) {
  return (
    <Grid container direction={"column"} spacing={2}>
      <Grid item>
        FILTERS
      </Grid>
      <Grid item>
        <TextField id="standard-basic" label="Email" variant="standard" />
      </Grid>
      <Grid item>
        <TextField id="standard-basic" label="Mobile" variant="standard" />
      </Grid>
      <Grid item>
        <TextField id="standard-basic" label="Name" variant="standard" />
      </Grid>
      <Grid item>
        <TextField id="standard-basic" label="Country" variant="standard" />
      </Grid>
      <Grid item>
        <TextField id="standard-basic" label="Date" variant="standard" />
      </Grid>
    </Grid>
  );
}

export default SideBar;
