import { Grid } from "@mui/material";
import { FC } from "react";
import SideBar from "./SideBar";
import TableData from "./TableData";


// type filterData = {
//   user_name: string,
//   user_phone: string,
//   country: string,
//   date: string
// }


const Layout: FC = () => {

  return (
    <Grid container spacing={2}>
      <Grid item xs={3}>
        <SideBar user_name={""} user_phone={""} country={""} date={""} />
      </Grid>
      <Grid item>
        <TableData />
      </Grid>
    </Grid>
  );
};

export default Layout;
